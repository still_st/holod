$(document).ready(function()
{
    $(document).ready(function(){
      $('.owl-carousel').owlCarousel({
        loop:true,
        items:1,
        nav:true,
        navText:['','']
      });
    });
});


//Popup-form
$(document).on('click', '#js-popup-form__close', function()
{
    $('#js-popup-form').slideToggle();
});

$(document).on('click', '.js-open-popup', function()
{
    $('#js-popup-form').slideToggle();
});

$(document).on('click', '#js-popup-form', function()
{
   if ($('#js-popup-form form:hover').length == 0){
       $("#js-popup-form").slideToggle();
   }
});

//nav-toogle
$(document).on('click', '.nav-toggle', function()
{
    $('.main-nav').slideToggle();
});

//FAQ
$(document).on('click', '.faq__q span', function()
{
    var btn = $(this);
    var question = btn.parent();
    var answer = question.next();
    question.toggleClass('faq__q--open');
    if (question.hasClass('faq__q--open')){
        btn.text('Закрыть ответ');
    }
    else{
        btn.text('Посмотреть ответ');
    }
    answer.slideToggle();
});

//COUNTER
var options = {
  useEasing : true,
  useGrouping : true,
  separator : ',',
  decimal : '.',
  prefix : '',
  suffix : ''
};

var exp = new CountUp('js-numbers__number--exp', 0, 5, 0, 5, options);
var spec = new CountUp('js-numbers__number--spec', 0, 32, 0, 3, options);
var ref = new CountUp('js-numbers__number--ref', 0, 2300, 0, 3, options);
var rate = new CountUp('js-numbers__number--rate', 0, 4.64, 2, 3, options);


$(document).on("scroll", function() {
    var distance = $('.numbers').offset().top - $(document).scrollTop();
    if (distance < 650 ){
        exp.start();
        spec.start();
        ref.start();
        rate.start();
    }
});