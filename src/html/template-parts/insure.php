<section class="sure">
    <div class="wrapper">
        <h2 class="sure__title">У нас всё надёжно</h2>
        <p class="sure__txt">
            Мы более 5 лет на рынке ремонта бытовой техники и специалируемся на ремонте холодильников. За это время мы сумели неоднократно доказать свой профессионализм а также добросовестное отношение к делу. Это подтверждается большим количеством наших довольных клиентов. Обращайтесь к нам, и ваша холодильник прослужит вам еще много много лет!
        </p>
        <div class="sure__facts">
            <div class="sure__facts-item sure__facts-item--check">
                <span class="sure__facts-title">
                    Компетентные сотрудники
                </span>
                <p class="sure__facts-txt">
                    Мы уделяем огромное внимание отбору и обучению наших мастеров. Нам крайне важна высокая квалификация наших специалистов, поэтому мы не жалеем времени и сил на их образование и стажировки!
                </p>
            </div>

            <div class="sure__facts-item sure__facts-item--insure">
                <span class="sure__facts-title">
                    Принимаем ответственность
                </span>
                <p class="sure__facts-txt">
                    Мы уверены в качестве своей работы и поэтому даём гарантию до двух лет на все виды ремонта проведёнными нашими специалистами и на используемые ими запчасти.
                </p>
            </div>
        </div>
        <div class="sure__img">
            <img src="img/refrigerator.png" alt="Красивенькая картинка холодильника">
        </div>
        <div class="sure__facts">
            <div class="sure__facts-item sure__facts-item--quality">
                <span class="sure__facts-title">
                    Следим за качеством
                </span>
                <p class="sure__facts-txt">
                    После каждого произведённого ремонта наши сотрудники из отдела контроля качества, внимательно вас выслушают и запишут все ваши пожелания и предложения. Каждый ваш отзыв помогает нам стать еще лучше.
                </p>
            </div>
            <div class="sure__facts-item sure__facts-item--arm">
                <span class="sure__facts-title">
                    Прозрачные цены на ремонт
                </span>
                <p class="sure__facts-txt">
                    В отличие от некоторых других компаний мы всегда максимально честны с нашими клиентами. Нам менеджер подробно вас расспросит о вашей поломке и сообщит примерную цену ремонта включая работу мастера и запчасти.
                </p>
            </div>
        </div>
    </div>
</section>