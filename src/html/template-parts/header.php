<header class="header">
    <div class="wrapper">
        <div class="logo">
            <a href="\">RemC</a><span class="js-open-popup cog"><img src="img/svg/cog.svg" alt=""></span><a href="\">ld</a>
        </div>
        <button class="nav-toggle" type="button">
            <span class="nav-toggle__bar"></span>
            <span class="nav-toggle__bar"></span>
            <span class="nav-toggle__bar"></span>
        </button>
        <nav class="main-nav">
            <ul>
                <li class="current-menu-item"><a href="#">О компании</a></li>
                <li><a href="#">Услуги</a></li>
                <li><a href="#">Контакты</a></li>
                <li><a href="#">Цены</a></li>
            </ul>
        </nav>
        <div class="tel-btn">
            <span class="tel-btn__tel">+7 (888) 888-88-88</span>
            <span class="js-open-popup tel-btn__btn">Заказать обратный звонок</span>
        </div>
    </div>
</header>