<section class="about-team">
        <div class="wrapper">
            <h2 class="about-team__title">
                Наша команда
            </h2>
            <ul>
                <li>
                    <div class="about-team__img">
                        <img src="../img/team/team-1.jpg" alt="Сотрудник компании">
                        <div class="about-team__txt">
                            <span>
                                Наталья
                            </span>
                            <p>Главный бухгалтер</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="about-team__img">
                        <img src="../img/team/team-1.jpg" alt="Сотрудник компании">
                        <div class="about-team__txt">
                            <span>
                                Наталья
                            </span>
                            <p>Главный бухгалтер</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="about-team__img">
                        <img src="../img/team/team-1.jpg" alt="Сотрудник компании">
                        <div class="about-team__txt">
                            <span>
                                Наталья
                            </span>
                            <p>Главный бухгалтер</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="about-team__img">
                        <img src="../img/team/team-1.jpg" alt="Сотрудник компании">
                        <div class="about-team__txt">
                            <span>
                                Наталья
                            </span>
                            <p>Главный бухгалтер</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="about-team__img">
                        <img src="../img/team/team-1.jpg" alt="Сотрудник компании">
                        <div class="about-team__txt">
                            <span>
                                Наталья
                            </span>
                            <p>Главный бухгалтер</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="about-team__img">
                        <img src="../img/team/team-1.jpg" alt="Сотрудник компании">
                        <div class="about-team__txt">
                            <span>
                                Наталья
                            </span>
                            <p>Главный бухгалтер</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="about-team__img">
                        <img src="../img/team/team-1.jpg" alt="Сотрудник компании">
                        <div class="about-team__txt">
                            <span>
                                Наталья
                            </span>
                            <p>Главный бухгалтер</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="about-team__img">
                        <img src="../img/team/team-1.jpg" alt="Сотрудник компании">
                        <div class="about-team__txt">
                            <span>
                                Наталья
                            </span>
                            <p>Главный бухгалтер</p>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
    </section>
