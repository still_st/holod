<section class="feedbacks">
    <div class="wrapper">
        <ul class="owl-carousel owl-theme feedbacks-list">
            <li>
                <img class="feedbacks-list__img" src="img/feedback/feedback_1.jpg" alt="Отзыв о компании">
                <div class="feedbacks-list__txt">
                    <cite class="feedbacks-list__name">
                        Гаврилов Станислав
                    </cite>
                    <blockquote class="feedbacks-list__q">
                        Нельзя сказать человеку: «Ты можешь творить.
                        Так давай, твори». Гораздо вернее подождать, пока он
                        сам не скажет: «Я могу творить, и я буду творить, хотите
                        вы этого или нет».
                    </blockquote>
                </div>
            </li>
            <li>
                <img class="feedbacks-list__img" src="img/feedback/feedback_2.jpg" alt="Отзыв о компании">
                <div class="feedbacks-list__txt">
                    <cite class="feedbacks-list__name">
                        Гендальф
                    </cite>
                    <blockquote class="feedbacks-list__q">
                        Нельзя сказать человеку: «Ты можешь творить.
                        Так давай, твори». Гораздо вернее подождать, пока он
                        сам не скажет: «Я могу творить, и я буду творить, хотите
                        вы этого или нет».
                    </blockquote>
                </div>
            </li>
            <li>
                <img class="feedbacks-list__img" src="img/feedback/feedback_3.jpg" alt="Отзыв о компании">
                <div class="feedbacks-list__txt">
                    <cite class="feedbacks-list__name">
                        Ричард Фейнман
                    </cite>
                    <blockquote class="feedbacks-list__q">
                        Нельзя сказаlть человеку: «Ты можешь творить.
                        Так давай, твори». Гораздо вернее подождать, пока он
                        сам не скажет: «Я могу творить, и я буду творить, хотите
                        вы этого или нет».
                    </blockquote>
                </div>
            </li>
        </ul>
    </div>
</section>