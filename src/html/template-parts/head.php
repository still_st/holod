<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700&subset=cyrillic" rel="stylesheet">
        <!-- build:css css/vendor.css -->
        <!-- bower:css -->
        <link href="bower/normalize-css/normalize.css" rel="stylesheet">
        <link href="bower/Font-Awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="bower/animate.css/animate.min.css" rel="stylesheet" >
        <link href="bower/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet" >
        <link href="bower/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
        <!-- endbower -->
        <!-- endbuild -->
        <!-- build:css css/style.css -->
        <link href="css/style.css" rel="stylesheet">
        <!-- endbuild -->
    </head>
    <body>