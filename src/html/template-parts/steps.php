 <div class="steps">
    <div class="wrapper">
        <div class="row">
            <div class="col-4 col-s-12">
                <div class="step step--one">
                    <span class="steps__title">
                        Закажите ремонт
                    </span>
                    <p class="steps__txt">
                        Расскажите о поломке, сообщите свой адрес, и удобное время для ремонта
                    </p>
                </div>
            </div>
            <div class="col-4 col-s-12">
                <div class="step step--two">
                    <span class="steps__title">
                        Дождитесь мастера
                    </span>
                    <p class="steps__txt">
                        Занимайтесь своими делами пока он будет заниматься ремонтом
                    </p>
                </div>
            </div>
            <div class="col-4 col-s-12">
                <div class="step step--three">
                    <span class="steps__title">
                        Платите за результат
                    </span>
                    <p class="steps__txt">
                        Принимайте работу мастера, оплачивайте и радуйтесь результату
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>