<div class="numbers">
    <div class="wrapper">
        <div class="row">
            <div class="col-3 col-m-6 col-s-12">
                <span id="js-numbers__number--exp" class="numbers__number">
                    5
                </span>
                <p class="numbers__txt">
                    лет опыта ремонта
                </p>
            </div>
            <div class="col-3 col-m-6 col-s-12">
                <span id="js-numbers__number--spec" class="numbers__number">
                    32
                </span>
                <p class="numbers__txt">
                    высококлассных специалиста
                </p>
            </div>
            <div class="col-3 col-m-6 col-s-12">
                <span id="js-numbers__number--ref" class="numbers__number">
                    2300
                </span>
                <p class="numbers__txt">
                    отремонтированных холодильника
                </p>
            </div>
            <div class="col-3 col-m-6 col-s-12">
                <span id="js-numbers__number--rate" class="numbers__number">
                    4,64
                </span>
                <p class="numbers__txt">
                    средний рейтинг обслуживания
                </p>
            </div>
        </div>
    </div>
</div>
