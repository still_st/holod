<section class="link-list">
    <div class="wrapper">
        <h2 class="link-list__title">
            В какой район можно вызвать мастера?
        </h2>
        <p class="link-list__sub-title">
            Наши сервис инженеры обслуживают все районы Санкт-Петербурга!
        </p>
        <div class="line">
            <div class="col-4">
                <ol class="left-list">
                    <li><a href="#">Адмиралтейский район</a></li>
                    <li><a href="#">Василеостровский район</a></li>
                    <li><a href="#">Выборгский район</a></li>
                    <li><a href="#">Калининский район</a></li>
                    <li><a href="#">Кировский район</a></li>
                    <li><a href="#">Колпинский район</a></li>
                    <li><a href="#">Красногвардейский район</a></li>
                    <li><a href="#">Красносельский район</a></li>
                    <li><a href="#">Кронштадтский район</a></li>

                </ol>
            </div>
            <div class="col-3">
                <ol start=10>
                    <li><a href="#">Курортный район</a></li>
                    <li><a href="#">Московский район</a></li>
                    <li><a href="#">Невский район</a></li>
                    <li><a href="#">Петроградский район</a></li>
                    <li><a href="#">Петродворцовый район</a></li>
                    <li><a href="#">Приморский район</a></li>
                    <li><a href="#">Пушкинский район</a></li>
                    <li><a href="#">Фрунзенский район</a></li>
                    <li><a href="#">Центральный район</a></li>
                </ol>
            </div>

            <div class="col-5">
                <div class="link-list__img">
                    <img src="img/districts.png" alt="Районы Санкт-Петербурга">
                </div>
            </div>
        </div>
    </div>
</section>