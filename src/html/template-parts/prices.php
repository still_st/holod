<section class="prices">
    <div class="wrapper">
        <h2 class="prices__title">
            Цены на ремонт холодильников на дому
        </h2>
        <table>
            <thead>
                <tr>
                    <th>Поломка</th>
                    <th>Цена</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Замена реле</td>
                    <td>700-1300</td>
                </tr>
                <tr>
                    <td>Замена термостата</td>
                    <td>600-1200</td>
                </tr>
                <tr>
                    <td>Ремонт электросхемы</td>
                    <td>700-1200</td>
                </tr>
                <tr>
                    <td>Замена нагревателя испарителя</td>
                    <td>800-1200</td>
                </tr>
                <tr>
                    <td>Устранение утечки хладогента</td>
                    <td>700-1400</td>
                </tr>
                <tr>
                    <td>Замена мотор-комрпессора</td>
                    <td>900-2500</td>
                </tr>
            </tbody>
        </table>
    </div>
</section>