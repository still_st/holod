 <!-- build:js js/vendor.js -->
    <!-- bower:js -->
    <script src="bower/jquery/dist/jquery.min.js"></script>
    <script src="bower/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="bower/countUp.js/dist/countUp.min.js"></script>
    <!-- endbower -->
    <!-- endbuild -->
    <!-- build:js js/script.js -->
    <script src="js/script.js">
    </script>
    <!-- endbuild -->
    </body>
</html>



