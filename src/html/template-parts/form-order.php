<div class="form-extra" id="form-extra">
    <div class="wrapper">
        <p class="form-extra__title">
           Доверьте проблему профессионалам
        </p>
        <p class="form-extra__sub-title">
            Оставьте заявку и мы перезвоним вам
        </p>
        <form action="#">
            <input type="text" placeholder="Имя">
            <input type="tel" placeholder="Телефон" required>
            <input type="submit" value="Вызвать мастера">
        </form>
    </div>
</div>