 <div class="advantages">
    <div class="wrapper">
        <div class="row">
            <div class="col-4 col-s-12">
                <div class="advantages__item advantages__item--quality">
                    <span class="advantages__item-title">
                        Следим за качеством
                    </span>
                    <p class="advantages__item-txt">После каждого ремонта выслушаем и запишем все ваши пожелания</p>
                </div>
            </div>
            <div class="col-4 col-s-12">
                <div class="advantages__item advantages__item--expert">
                    <span class="advantages__item-title">Компетентные сотрудники</span>
                    <p class="advantages__item-txt">Не жалеем времени и сил на образование и стажировки наших сотрудников</p>
                </div>
            </div>
            <div class="col-4 col-s-12">
                <div class="advantages__item advantages__item--responsibility">
                    <span class="advantages__item-title">Несём ответсвенность</span>
                    <p class="advantages__item-txt">Даём гарантию до двух лет на все виды ремонта и на используемые запчасти</p>
                </div>
            </div>
        </div>
    </div>
</div>