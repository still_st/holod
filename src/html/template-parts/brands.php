<section class="service-company">
    <div class="wrapper">
        <h2 class="service-company__title">
            Ремонт холодильников любой модели
        </h2>
        <ul class="service-company__list">
            <li>
                <a href="#"><img src="img/companies/aeg.jpg" alt="Ремонт холодильников Aeg (Аег) в Санкт-Петербурге" title="Ремонт холодильников Aeg (Аег) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/ardo.jpg" alt="Ремонт холодильников Ardo (Ардо) в Санкт-Петербурге" title="Ремонт холодильников Ardo (Ардо) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/asko.jpg" alt="Ремонт холодильников Asko (Аско) в Санкт-Петербурге" title="Ремонт холодильников Asko (Аско) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/ariston.jpg" alt="Ремонт холодильников Ariston (Аристон) в Санкт-Петербурге" title="Ремонт холодильников Ariston (Аристон) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/amana.jpg" alt="Ремонт холодильников Amana (Амана) в Санкт-Петербурге" title="Ремонт холодильников Amana (Амана) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/bauknecht.jpg" alt="Ремонт холодильников Bauknecht (Баукнет) в Санкт-Петербурге" title="Ремонт холодильников Bauknecht (Баукнет) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/beko.jpg" alt="Ремонт холодильников Beko (Беко) в Санкт-Петербурге" title="Ремонт холодильников Beko (Беко) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/bosch.jpg" alt="Ремонт холодильников Bosch (Бош) в Санкт-Петербурге" title="Ремонт холодильников Bosch (Бош) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/brandt.jpg" alt="Ремонт холодильников Brandt (Бранд) в Санкт-Петербурге" title="Ремонт холодильников Brandt (Бранд) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/candy.jpg" alt="Ремонт холодильников Candy (Канди) в Санкт-Петербурге" title="Ремонт холодильников Brandt (Бранд) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/daewoo.jpg" alt="Ремонт холодильников Daewoo (Дэу) в Санкт-Петербурге" title="Ремонт холодильников Daewoo (Дэу) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/electrolux.jpg" alt="Ремонт холодильников Electrolux (Электролюкс) в Санкт-Петербурге" title="Ремонт холодильников Electrolux (Электролюкс) в Санкт-Петербурге">
                </a>
            </li>
            <li>
                <a href="#"><img src="img/companies/frigidaire.jpg" alt="Ремонт холодильников Frigidaire (Фриджидайр) в Санкт-Петербурге" title="Ремонт холодильников Frigidaire (Фриджидайр) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/general-electric.jpg" alt="Ремонт холодильников General Electric (Дженерал Электрик) в Санкт-Петербурге" title="Ремонт холодильников General Electric (Дженерал Электрик) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/gorenje.jpg" alt="Ремонт холодильников Gorenje (Горение) в Санкт-Петербурге" title="Ремонт холодильников Gorenje (Горение) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/hansa.jpg" alt="Ремонт холодильников Hansa (Ханса) в Санкт-Петербурге" title="Ремонт холодильников Hansa (Ханса) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/indesit.jpg" alt="Ремонт холодильников Indesit (Индезит) в Санкт-Петербурге" title="Ремонт холодильников Indesit (Индезит) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/kaiser.jpg" alt="Ремонт холодильников Kaiser (Кайзер) в Санкт-Петербурге" title="Ремонт холодильников Kaiser (Кайзер) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/liebherr.jpg" alt="Ремонт холодильников  Liebherr (Либхер) в Санкт-Петербурге" title="Ремонт холодильников  Liebherr (Либхер) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/LG.jpg" alt="Ремонт холодильников  LG (Элджи) в Санкт-Петербурге" title="Ремонт холодильников  LG (Элджи) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/miele.jpg" alt="Ремонт холодильников  Miele (Миле) в Санкт-Петербурге" title="Ремонт холодильников  Miele (Миле) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/philco.jpg" alt="Ремонт холодильников  Philco (Филко) в Санкт-Петербурге" title="Ремонт холодильников  Philco (Филко) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/rosenlew.jpg" alt="Ремонт холодильников  Rosenlew (Розенлев) в Санкт-Петербурге" title="Ремонт холодильников  Rosenlew (Розенлев) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/samsung.jpg" alt="Ремонт холодильников  Samsung (Самсунг) в Санкт-Петербурге" title="Ремонт холодильников  Samsung (Самсунг) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/shivaki.jpg" alt="Ремонт холодильников  Shivaki (Шиваки) в Санкт-Петербурге" title="Ремонт холодильников  Shivaki (Шиваки) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/sharp.jpg" alt="Ремонт холодильников Sharp (Шарп) в Санкт-Петербурге" title="Ремонт холодильников Sharp (Шарп) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/siemens.jpg" alt="Ремонт холодильников Siemens (Сименс) в Санкт-Петербурге" title="Ремонт холодильников Siemens (Сименс) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/siltal.jpg" alt="Ремонт холодильников Siltal (Силтал) в Санкт-Петербурге" title="Ремонт холодильников Siltal (Силтал) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/stinol.jpg" alt="Ремонт холодильников Stinol (Стинол) в Санкт-Петербурге" title="Ремонт холодильников Stinol (Стинол) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/vestfrost.jpg" alt="Ремонт холодильников Vestfrost (Вестфрост) в Санкт-Петербурге" title="Ремонт холодильников Vestfrost (Вестфрост) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/vestel.jpg" alt="Ремонт холодильников Vestel (Вестэль) в Санкт-Петербурге" title="Ремонт холодильников Vestel (Вестэль) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/viking.jpg" alt="Ремонт холодильников Viking (Викинг) в Санкт-Петербурге" title="Ремонт холодильников Viking (Викинг) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/whirlpool.jpg" alt="Ремонт холодильников Whirlpool (Вирпул) в Санкт-Петербурге" title="Ремонт холодильников Whirlpool (Вирпул) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/zanussi.jpg" alt="Ремонт холодильников Zanussi (Занусси) в Санкт-Петербурге" title="Ремонт холодильников Zanussi (Занусси) в Санкт-Петербурге">
                </a>
            </li>

            <li>
                <a href="#"><img src="img/companies/zerowatt.jpg" alt="Ремонт холодильников Zerowatt (Зероват) в Санкт-Петербурге" title="Ремонт холодильников Zerowatt (Зероват) в Санкт-Петербурге">
                </a>
            </li>
        </ul>
        <ul class="service-company__ruslist">
            <li>
                <b>Отечественные:</b>
            </li>
            <li>
                <a href="#">Атлант</a>,
            </li>
            <li>
                <a href="#">Днепр</a>,
            </li>
            <li>
                <a href="#">Донбасс</a>,
            </li>
            <li>
                <a href="#">Зил</a>,
            </li>
            <li>
                <a href="#">Норд</a>,
            </li>
            <li>
                <a href="#">Ока</a>,
            </li>
            <li>
                <a href="#">Орск</a>,
            </li>
            <li>
                <a href="#">Саратов</a>,
            </li>
            <li>
                <a href="#">Свияга</a>,
            </li>
            <li>
                <a href="#">Кавказ</a>,
            </li>
            <li>
                <a href="#">Юрюзань</a>,
            </li>
            <li>
                и др.
            </li>
        </ul>
    </div>
</section>